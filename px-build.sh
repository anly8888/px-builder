#!/bin/bash
set -e

fatal()
{
    echo -e "FATAL: " $@
    exit 1
}

usage() 
{
    fatal "Usage: $0 <src_dir>"
}

[ ! $# -lt 1 ] || usage

SOURCE_DIR=${1}
BINARY_DIR="$PWD"
TARGET_DIR=oem

export HOST_DIR="$(cd $(dirname $(realpath $BASH_SOURCE))/.. && pwd)"
export PATH="$PATH:$HOST_DIR/bin"

echo "========================================"
echo "SOURCE_DIR = $SOURCE_DIR"
echo "BINARY_DIR = $BINARY_DIR"
echo "TARGET_DIR = $TARGET_DIR"
echo "HOST_DIR   = $HOST_DIR"
echo "========================================"

# configure
echo "Configuring ..."
rm -f CMakeCache.txt
cmake $SOURCE_DIR \
    -GNinja \
    -DCMAKE_TOOLCHAIN_FILE="$HOST_DIR/share/buildroot/toolchainfile.cmake" \
    -DCMAKE_INSTALL_PREFIX="/usr/local/px" \
    -DCMAKE_BUILD_TYPE=RelWithDebInfo \
    -DCMAKE_COLOR_MAKEFILE=OFF \
    -DBUILD_DOC=OFF \
    -DBUILD_DOCS=OFF \
    -DBUILD_EXAMPLE=OFF \
    -DBUILD_EXAMPLES=OFF \
    -DBUILD_TEST=OFF \
    -DBUILD_TESTS=OFF \
    -DBUILD_TESTING=OFF \
    -DBUILD_SHARED_LIBS=ON

# build
echo "Building ..."
cmake --build .

# install
echo "Installing ..."
rm -rf $TARGET_DIR
DESTDIR=$TARGET_DIR cmake --install .

# generate symbols
echo "Generating symbols ..."
$HOST_DIR/bin/gen-syms.sh $BINARY_DIR $TARGET_DIR \
    /usr/local/px/NetDevice/txtexport.so \
    /usr/local/px/instrument \
    /usr/local/px/libnxbase.so \
    /usr/local/px/libnxcore.so \
    /usr/local/px/poctcoreapp \
    /usr/local/px/pocthttpinstr.so \
    /usr/local/px/pocthttpserver.so \
    /usr/local/px/pui \
    /usr/local/px/updb

# strip
echo "Stripping ..."
find "$TARGET_DIR" -type f \
    \( -perm /111 -o -name '*.so*' \) -not \
    \( -name 'libpthread*.so*' -o -name 'ld-*.so*' -o -name '*.ko' \) \
    -print0 | \
    xargs -0 $HOST_DIR/bin/aarch64-buildroot-linux-gnu-strip \
    --remove-section=.comment \
    --remove-section=.note 2>/dev/null || true

# make oem.img
echo "Generating Image/oem.img ..."
mkdir -p "Image"
OEM_FAKEROOT_SCRIPT=$(mktemp)
MKIMAGE="$HOST_DIR/bin/mk-image.sh"
echo "#!/bin/sh" > $OEM_FAKEROOT_SCRIPT
echo "set -e" >> $OEM_FAKEROOT_SCRIPT
if [ -d $TARGET_DIR/www ]; then
    echo "chown -R www-data:www-data $TARGET_DIR/www" >> $OEM_FAKEROOT_SCRIPT
fi
echo "$MKIMAGE $TARGET_DIR Image/oem.img ext2" >> $OEM_FAKEROOT_SCRIPT
chmod a+x $OEM_FAKEROOT_SCRIPT
fakeroot -- $OEM_FAKEROOT_SCRIPT
rm -f $OEM_FAKEROOT_SCRIPT

# make update.img
echo "Generating Image/update.img ..."
ln -svf "$HOST_DIR/usr/share/px-builder/MiniLoaderAll.bin" Image/
ln -svf "$HOST_DIR/usr/share/px-builder/parameter.txt" Image/
ln -svf "$HOST_DIR/usr/share/px-builder/package-file" .
$HOST_DIR/bin/mkupdate.sh
